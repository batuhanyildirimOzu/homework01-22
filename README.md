
public class teaCup {
			double liters;
			int temp;
			boolean lemon;
		
			public teaCup(double d, int i, boolean b) {
				liters = d;
				temp = i;
				lemon = b;
			}
		
			boolean a;
		
			public void methodA() {
				if (liters < 0.2) {
					System.out.print("more tea! ");
					double a = 0.2 - liters;
					System.out.println("add " + a);
				}
			}
		
			public void methodB() {
				if (liters > 0.2) {
					System.out.println("less tea!");
					double a = liters - 0.2;
					System.out.println("pour " + a);
		
				}
			}
		
			public void methodC() {
				if (temp < 30) {
					System.out.println("its cold");
					int a = 30 - temp;
					System.out.println("it need to get hotter by " + a + "C degrees");
				} else if (temp > 30) {
					System.out.println("hot!");
					int a = temp - 30;
					System.out.println("it need to get colder by " + a + "C degrees");
				}
			}
		
			public void methodD() {
				if (lemon == false) {
					System.out.println("put lemon");
				}
			}
		
		public boolean methodE() {
		if (liters == 0.2 && temp == 30 && lemon== true ) {	
		    a = true;
		    
		}else {
		
		
			a = false;
			
		}
		return a;
		}
		public void report() {
			System.out.println(a);
		}
		}



public class Main {
public static void main(String[] args) {
	teaCup teaCup1 = new teaCup(0.1 , 30, false);
	teaCup teaCup2 = new teaCup(0.1 , 50 , false);
	teaCup teaCup3 = new teaCup(0.2, 30 , false);
	teaCup teaCup4 = new teaCup(0.2, 30 , true);
	
	
	teaCup1.report();
	teaCup2.report();
	teaCup3.report();
	teaCup4.report();
	
}
}
